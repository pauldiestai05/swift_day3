//
//  LoginPageView.swift
//  MiniBlog
//
//  Created by Seer on 10/6/15.
//  Copyright © 2015 Seer Technologies, Inc. All rights reserved.
//

import Foundation
import UIKit

protocol LoginPageViewProtocol{

}


class LoginPageView: BaseView, GIDSignInUIDelegate{
    
    
    var delegate:LoginPageViewProtocol?
    
   @IBOutlet weak var signInButton: GIDSignInButton!
    
    @IBAction func didTapSignOut(sender: AnyObject) {
        GIDSignIn.sharedInstance().signOut()
    }
    
//    @IBAction func ggleSignInBtnPressed(sender: AnyObject) {
//        
//        if self.delegate != nil && self.delegate?.ggleSignInBtnPressed != nil{
//            self.delegate?.ggleSignInBtnPressed()
//            
//        }
//
//    }
    
}