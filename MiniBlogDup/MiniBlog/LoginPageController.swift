//
//  LoginPageController.swift
//  MiniBlog
//
//  Created by Allister Alambra on 10/6/15.
//  Copyright © 2015 Seer Technologies, Inc. All rights reserved.
//
import Foundation
import UIKit
import CoreData



class LoginPageController: BaseController, LoginPageViewProtocol, GIDSignInUIDelegate{
    
    var loginView: LoginPageView?
    var session:Sessions?
    
    // MARK: Lifecycle method override
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //for ggle sign in
        //GIDSignIn.sharedInstance().delegate = self
        GIDSignIn.sharedInstance().uiDelegate = self
        
        GIDSignIn.sharedInstance().clientID = "673614192082-8aeu6ejb67apankf9kp3sufp0ueq69cj.apps.googleusercontent.com"
        
        // Load Targeted Xibs
        self.loadXibName("LoginPageView")
        self.loginView = (self.view as! LoginPageView)
        self.loginView?.delegate = self
        
        // Initialize UI Elements
        //self.initializeNavigationBar()
        
        NSNotificationCenter.defaultCenter().addObserver(self,
            selector: "notifReceiver:",
            name: "notifAuthentication",
            object: nil)
        
        
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    // MARK: Controller Initializers
    
    func initializeNavigationBar(){
        
        let addBlogButtonItem = UIBarButtonItem(title: "Blog Now!", style: .Plain, target: self, action: Selector("addBlogPost"))
        
        self.navigationItem.rightBarButtonItem = addBlogButtonItem
        
    }
        
    func signInWillDispatch(signIn: GIDSignIn!, error: NSError!) {
        print("processing")
        //myActivityIndicator.stopAnimating()
    }
    
    // Present a view that prompts the user to sign in with Google
    func signIn(signIn: GIDSignIn!,
        presentViewController viewController: UIViewController!) {
            self.presentViewController(viewController, animated: true, completion: nil)
            
            //self.performSegueWithIdentifier("loginToHomeSegue", sender: self)
    }
    
    // Dismiss the "Sign in with Google" view
    func signIn(signIn: GIDSignIn!,
        dismissViewController viewController: UIViewController!) {
            self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    deinit {
        NSNotificationCenter.defaultCenter().removeObserver(self,
            name: "notifAuthentication",
            object: nil)
    }
    
    func notifReceiver(notification: NSNotification) {
        if (notification.name == "notifAuthentication") {
            
            if notification.userInfo != nil {
                let userInfo:Dictionary<String,String!> =
                notification.userInfo as! Dictionary<String,String!>
                print(userInfo["loggedStatus"])
                redirectToHome()
            }
        }
    }
    
    func redirectToHome()
    {
        self.performSegueWithIdentifier("loginToHomeSegue", sender: self)
    }

    

    
}

