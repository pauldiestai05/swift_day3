//
//  ViewController.swift
//  MiniBlog
//
//  Created by Allister Alambra on 10/6/15.
//  Copyright © 2015 Seer Technologies, Inc. All rights reserved.
//

import UIKit
import CoreData

class HomeController: BaseController, UITableViewDataSource, UITableViewDelegate{

    var homeView: HomeView?
    var BlogPost: [Post]?
    
    // MARK: Lifecycle method override
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Load Targeted Xibs
        self.loadXibName("HomeView")
        self.homeView = (self.view as! HomeView)
        self.homeView?.blogTable.dataSource = self
        self.homeView?.blogTable.delegate = self
        
        // Initialize UI Elements
        self.initializeNavigationBar()
        
        // Register Custom TableView Class
        let nibName = UINib(nibName: "BlogPostCell", bundle:nil)
        self.homeView?.blogTable.registerNib(nibName, forCellReuseIdentifier: "BlogPostCell")
        
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        let managedContext = appDelegate.managedObjectContext
        let fetchRequest = NSFetchRequest(entityName: "Post")
        do {
            let results = try managedContext.executeFetchRequest(fetchRequest)
            self.BlogPost = results as? [Post]
            self.homeView?.blogTable.reloadData()
        } catch let error as NSError {
            print("Could not fetch \(error), \(error.userInfo)")
        }
        
    }
    
    // MARK: Segue to AddBlogView
    func addBlogPost(){
        self.performSegueWithIdentifier("HomeToAddBlogSegue", sender: self)
    }
    
    func signOut(){
        GIDSignIn.sharedInstance().signOut()
        self.dismissViewControllerAnimated(true, completion: {})
        let alert = UIAlertController(title: "Alert", message: "Successfully Signed Out", preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
        self.presentViewController(alert, animated: true, completion: nil)
        //exit(0)
        //self.performSegueWithIdentifier("HomeToAddBlogSegue", sender: self)
    }
    

    
    // MARK: Controller Initializers
    
    func initializeNavigationBar(){
        
        let addBlogButtonItem = UIBarButtonItem(title: "Blog Now!", style: .Plain, target: self, action: Selector("addBlogPost"))
        
        let signOutBtn = UIBarButtonItem(title: "Sign Out!", style: .Plain, target: self, action: Selector("signOut"))
        
        self.navigationItem.leftBarButtonItem = signOutBtn
        self.navigationItem.rightBarButtonItem = addBlogButtonItem
        
    }

    // MARK: UITableViewDataSource Methods
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return self.BlogPost!.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell{
        
        let cell = self.homeView?.blogTable.dequeueReusableCellWithIdentifier("BlogPostCell", forIndexPath: indexPath) as! BlogPostCell
        
        let BlogPost:Post = self.BlogPost![indexPath.row]
        
        cell.blogTitleLabel.text = BlogPost.title! + " by " + BlogPost.post_to_author!.name!
        cell.blogPreviewLabel.text = BlogPost.content!
        cell.blogDateLabel.text = String(BlogPost.date!)
        
        return cell
    }
    
    // MARK: UITableViewDelegate Methods
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat{
        return 70.0
    }
    
}

